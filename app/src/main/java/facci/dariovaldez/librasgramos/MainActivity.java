package facci.dariovaldez.librasgramos;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button btnConvertir;
    EditText txtValorLibras;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtValorLibras = (EditText) findViewById(R.id.txtValorLibras);
        btnConvertir = (Button) findViewById(R.id.btnConvertir);

        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ConversionActivity.class);
                Bundle bundle = new Bundle();
                float flot = Float.parseFloat(txtValorLibras.getText().toString());
                bundle.putFloat("valor", flot);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
