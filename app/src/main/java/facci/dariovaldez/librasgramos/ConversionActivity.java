package facci.dariovaldez.librasgramos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class ConversionActivity extends AppCompatActivity {

    TextView lblResultado;

    final float valorConvertir = (float) 453.592;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);

        lblResultado = (TextView) findViewById(R.id.lblResultado);
        Bundle bundle = this.getIntent().getExtras();
        Float gramos = bundle.getFloat("valor");
        Float resultado = (gramos * valorConvertir);
        String resultadoString = Float.toString(resultado);
        lblResultado.setText(resultadoString + " gr");
        Toast.makeText(this, "Regresa a la pantalla anterior para otra conversión", Toast.LENGTH_LONG).show();
    }
}
